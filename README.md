## Abpx Doc 文档生成

[Documentation](https://abpx.gitee.io/abpx-doc/)

### 介绍

@abpx/doc 是**Abpx 团队**开源的基于 markerdown,使用 vue 快速构建页面，发布至 Gitee Pages 生成在线文档,开源项目必备工具 文档样式内容节选至[Vant 有赞](https://youzan.github.io/vant)

### 快速上手

请参考<a href="https://abpx.gitee.io/abpx-doc/#/zh-CN/quickstart">快速上手</a>章节。

### 贡献代码

修改代码请阅读我们的<a href="https://abpx.gitee.io/abpx-doc/#/zh-CN/dev-guide">开发指南</a>。

使用过程中发现任何问题都可以提 Issue 给我们，当然，我们也非常欢迎你给我们发 PR。

### 生态

| 项目                                             | 描述                                                                   |
| ------------------------------------------------ | ---------------------------------------------------------------------- |
| [@abpx/vue](https://gitee.com/abpx/abpx-vue.git) | Vue 应用框架                                                           |
| [@abpx/doc](https://gitee.com/abpx/abpx-doc.git) | 基于 markerdown,使用 vue 快速构建页面，发布至 Gitee Pages 生成在线文档 |

### 感谢开源项目

[https://youzan.github.io/vant](https://youzan.github.io/vant)

[https://github.com/markdown-it](https://github.com/markdown-it)

[https://github.com/QingWei-Li/vue-markdown-loader](https://github.com/QingWei-Li/vue-markdown-loader)
