import { createAbpxDoc } from './create-abpx-doc'
import { docSetting } from './doc-setting'

export default createAbpxDoc

export { docSetting }
