export type DocSetting = {
  routePrefix: string
  menus: Array<Menu>
  logo: string
}

export type Menu = {
  title: string
  children: Array<SubMenu>
}

export type SubMenu = {
  title: string
  path: string
  component: Vue
}
