import { DocSetting } from './doc-setting'
import VueRouter from 'vue-router'

export type CreateAbpxDoc = (router: VueRouter, docSetting: DocSetting) => void
