import Vue from 'vue'
import App from './App.vue'
import router from './router'
import '@abpx/doc/theme/style.less'
import createAbpxDoc from '@abpx/doc'
import menus from './router/menus'

Vue.config.productionTip = false

createAbpxDoc(router, {
  package: require('../package.json'),
  logo: require('./assets/logo.png'),
  menus: menus
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
