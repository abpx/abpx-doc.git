export default [
  {
    title: '开发指南',
    children: [
      {
        path: '/home',
        component: () => import('@/docs/home.md'),
        title: '介绍'
      },
      {
        path: '/changelog',
        component: () => import('@/docs/change-log.md'),
        title: '更新日志'
      },
      {
        path: '/quickstart',
        component: () => import('@/docs/quickstart.md'),
        title: '快速上手'
      },
      {
        path: '/dev-guide',
        component: () => import('@/docs/dev-guide.md'),
        title: '开发指南'
      },
      {
        path: '/style-guide',
        component: () => import('@/docs/style-guide.md'),
        title: '风格指南'
      },
      {
        path: '/team',
        component: () => import('@/docs/team.md'),
        title: '团队介绍'
      }
    ]
  },
  {
    title: '全局函数',
    children: [
      {
        path: '/create-abpx-doc',
        component: () => import('@/docs/create-abpx-doc.md'),
        title: 'createAbpxDoc'
      }
    ]
  },
  {
    title: 'Markdown',
    children: [
      {
        path: '/markdown',
        component: () => import('@/markdown/README.md'),
        title: 'Markdown语法'
      }
    ]
  }
]
