export default {
  logo: require('../theme/images/logo.png'),
  routePrefix: 'zh-CN',
  menus: [],
  package: require('../../package.json')
}
