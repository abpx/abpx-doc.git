# 开发指南

### 介绍

感谢你使用 {{$docSetting.package.name}}。指南内容节选自 [Vant 开发指南](https://youzan.github.io/vant/#/zh-CN/contribution)

以下是关于向 {{$docSetting.package.name}} 提交反馈或代码的指南。在向 {{$docSetting.package.name}} 提交 issue 或者 PR 之前，请先花几分钟时间阅读以下文字。

### Issue 规范

- 遇到问题时，请先确认这个问题是否已经在 issue 中有记录或者已被修复
- 提 issue 时，请用简短的语言描述遇到的问题，并添加出现问题时的环境和复现步骤

## 参与开发

### 本地开发

按照下面的步骤操作，即可在本地开发 {{$docSetting.package.name}}

```bash
# 克隆仓库
git clone https://gitee.com/abpx/abpx-doc.git

# 安装依赖
cd doc && npm i

# 进入开发模式，浏览器访问 http://localhost:8080
npm run serve
```

## 提交 RP

### Pull Request 规范

- 如果遇到问题，建议保持你的 PR 足够小。保证一个 PR 只解决一个问题或只添加一个功能
- 在 PR 中请添加合适的描述，并关联相关的 Issue

### Pull Request 流程

- fork 主仓库，如果已经 fork 过，请同步主仓库的最新代码
- 基于 fork 后仓库的 dev 分支新建一个分支，比如 feature/language
- 在新分支上进行开发，开发完成后，提 Pull Request 到主仓库的 dev 分支
- Pull Request 会在 Review 通过后被合并到主仓库
- 等待 @abpx/doc 发布版本，一般是每周一次

### 同步最新代码

提 Pull Request 前，请依照下面的流程同步主仓库的最新代码

```bash
# 添加主仓库到 remote，作为 fork 后仓库的上游仓库
git remote add upstream https://gitee.com/abpx/abpx-doc.git

# 拉取主仓库最新代码
git fetch upstream

# 切换至 dev 分支
git checkout dev

# 合并主仓库代码
git merge upstream/dev
```
