### 介绍

{{$docSetting.package.name}} 是**Abpx 团队**开源的 Markdown 文件 快速构建文档的工具库, 文档样式内容节选至[Vant 有赞](https://youzan.github.io/vant)

本项目基于@abpx/doc 文档生成，发布至 Gitee Pages 在线文档

### 快速上手

请参考<a href="#/zh-CN/quickstart">快速上手</a>章节。

### 贡献代码

修改代码请阅读我们的<a href="#/zh-CN/dev-guide">开发指南</a>。

使用过程中发现任何问题都可以提 Issue 给我们，当然，我们也非常欢迎你给我们发 PR。

### 生态

| 项目                                             | 描述                                      |
| ------------------------------------------------ | ----------------------------------------- |
| [@abpx/vue](https://gitee.com/abpx/abpx-vue.git) | Vue 应用框架                              |
| [@abpx/doc](https://gitee.com/abpx/abpx-doc.git) | 文档生成，发布至 Gitee Pages 生成在线文档 |

### 感谢开源项目

[https://youzan.github.io/vant](https://youzan.github.io/vant)

[https://github.com/markdown-it](https://github.com/markdown-it)

[https://github.com/QingWei-Li/vue-markdown-loader](https://github.com/QingWei-Li/vue-markdown-loader)
