# 快速上手

### 通过 npm 安装

在现有项目中使用 @apbx/doc 时，可以通过`npm`或`yarn`安装

```bash
# 通过 npm 安装
npm i @abpx/doc -D

# 通过 yarn 安装
yarn add @abpx/doc
```

## 引入组件

### 方式一：完整引入,托管方式

在 main.js 中写入以下内容

```js
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import '@abpx/doc/lib/theme/style.less'
import createAbpxDoc from '@abpx/doc'
import menus from './router/menus'

Vue.config.productionTip = false

createAbpxDoc(router, {
  menus: menus,
  package: require('../package.json'),
  logo: require('./assets/logo.png')
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
```

在 menu.js 文件写入以下内容

```js
export default [
  {
    title: '开发指南',
    children: [
      {
        path: '/home',
        component: () => import('@/docs/home.md'),
        title: '介绍'
      },
      {
        path: '/changelog',
        component: () => import('@/docs/change-log.md'),
        title: '更新日志'
      },
      {
        path: '/quickstart',
        component: () => import('@/docs/quickstart.md'),
        title: '快速上手'
      },
      {
        path: '/dev-guide',
        component: () => import('@/docs/dev-guide.md'),
        title: '开发指南'
      },
      {
        path: '/style-guide',
        component: () => import('@/docs/style-guide.md'),
        title: '风格指南'
      },
      {
        path: '/team',
        component: () => import('@/docs/team.md'),
        title: '团队介绍'
      }
    ]
  }
]
```

在 vue.config.js 添加如下配置

```js
module.exports = {
  chainWebpack: config => {
    config.module
      .rule('md')
      .test(/\.md/)
      .use('@abpx/doc/lib/markdown/loader')
      .loader('@abpx/doc/lib/markdown/compiler')
  }
}
```
