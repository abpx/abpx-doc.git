# 团队介绍

### 介绍

Abpx 开源团队是一个初创开源团队，一群可爱的小伙伴一拍即合成立的团队，现有成员 3 人,现需要一名 java 开发者加入，欢迎报名

##

<div class="team">
    <div class="team-item">
        <div class="team-item--avatar male">
        </div>
        <div class="team-item--name">
            smilecoder
        </div>
        <div class="team-item--intro">
            全栈工程师,精通Vue、小程序、Flutter,熟练使用.Net后端开发
        </div>
    </div>
    <div class="team-item">
        <div class="team-item--avatar female">
        </div>
        <div class="team-item--name">
            Nancy
        </div>
        <div class="team-item--intro">
            前端工程师，熟练使用Vue、小程序、H5、Typescript开发
        </div>
    </div>
    <div class="team-item">
        <div class="team-item--avatar male">
        </div>
        <div class="team-item--name">
            Jacker
        </div>
        <div class="team-item--intro">
            运维工程师，熟练Linux环境部署，Docker、K8s、阿里云、腾讯云
        </div>
    </div>
</div>
