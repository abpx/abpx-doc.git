# createAbpxDoc 函数

### 引入

```js
import createAbpxDoc from '@abpx/doc'
createAbpxDoc(router, {
  package: require('../package.json'),
  logo: require('./assets/logo.png'),
  menus: [
    {
      title: '开发指南',
      children: [
        {
          title: '介绍',
          path: '/home',
          component: () => import('@/docs/home.md')
        }
      ]
    }
  ]
})
```

### 参数

| 参数   | 说明     | 类型         | 默认值 |
| ------ | -------- | ------------ | ------ |
| router | 路由对象 | _Router_     | -      |
| config | 配置     | _DocSetting_ | -      |

### AbpxConfig Props

| 参数        | 说明                                                                           | 类型     | 默认值       |
| ----------- | ------------------------------------------------------------------------------ | -------- | ------------ |
| routePrefix | 路由前缀                                                                       | _string_ | zh-CN        |
| package     | 包的信息                                                                       | _object_ | Abpx 包信息  |
| logo        | logo 图片,如加载本地图片:<span class="text-primary">require('logo.png')</span> | _string_ | Abpx 的 logo |
| menus       | 菜单                                                                           | _Menu[]_ | []           |

### Menu Props

| 参数     | 说明     | 类型        | 默认值 |
| -------- | -------- | ----------- | ------ |
| title    | 菜单名称 | _string_    | -      |
| children | 子菜单   | _SubMenu[]_ | -      |

### Sub Menu Props

| 参数      | 说明     | 类型     | 默认值 |
| --------- | -------- | -------- | ------ |
| title     | 菜单名称 | _string_ | -      |
| path      | 菜单路由 | _string_ | -      |
| component | 组件     | _Vue_    | -      |
