const path = require('path')
function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  runtimeCompiler: true,
  pages: {
    index: {
      title: '开发文档',
      entry: 'docs/main.js',
      chunks: ['chunk-vendors', 'chunk-common', 'index']
    }
  },
  publicPath: process.env.NODE_ENV === 'production' ? '/abpx-doc' : '/',
  outputDir: 'release/docs',
  productionSourceMap: false,
  chainWebpack: config => {
    config.resolve.alias.set('@abpx/doc', resolve('lib'))
    config.resolve.alias.set('@', resolve('lib'))
    config.module
      .rule('md')
      .test(/\.md/)
      .use('vue-loader')
      .loader('vue-loader')
      .end()
      .use(resolve('lib/markdown/loader'))
      .loader(resolve('lib/markdown/compiler'))
  }
}
